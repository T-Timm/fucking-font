; --------------------------------
;  Includes
; --------------------------------

!include LogicLib.nsh
!include nsDialogs.nsh

; --------------------------------
;  Variables
; --------------------------------


!define name "{{name}}"
!define productName "{{productName}}"
!define version "{{version}}"
!define icon "{{icon}}"
!define setupIcon "{{setupIcon}}"
!define banner "{{banner}}"

!define regkey "Software\${productName}"
!define uninstkey "Software\Microsoft\Windows\CurrentVersion\Uninstall\${productName}"

!define uninstaller "uninstall.exe"

; --------------------------------
;  Installation
; --------------------------------


Unicode true
SetCompressor /SOLID lzma
Name "${productName}"
Icon "${setupIcon}"
OutFile "${dest}"
InstallDir "$PROGRAMFILES\${productName}"
InstallDirRegKey HKLM "${regkey}" ""

;   --------------------------------
;    Page layout
;   --------------------------------

Page custom welcome
Page components
Page directory
Page instfiles
Page custom finish finishEnd

;   --------------------------------
;    Welcome page [custom]
;   --------------------------------
